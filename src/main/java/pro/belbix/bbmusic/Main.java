package pro.belbix.bbmusic;

import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.LSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {
    private static Random random = new Random(12345);
    private static final String ROOT_PATH = "/trainsets/music/";

    public static void main(String[] args) {
        String pathForSaveNet = ROOT_PATH + "net";

        int lstmLayerSize = 1000;                    //Number of units in each LSTM layer
        int miniBatchSize = 10000;                        //Size of mini batch to use when  training
        int exampleLength = 10000;                    //Length of each training example sequence to use. This could certainly be increased
        int tbpttLength = 50;                       //Length for truncated backpropagation through time. i.e., do parameter updates ever 50 characters
        int numEpochs = 100;                            //Total number of training epochs
        int generateSamplesEveryNMinibatches = 10;  //How frequently to generate samples from the network? 1000 characters / 50 tbptt length: 20 parameter updates per minibatch
        int nSamplesToGenerate = 1;                    //Number of samples to generate after each training epoch
        int nPricesToSample = 100000;                //Length of each sample to generate
        short[] generationInitialization = null;        //Optional character initialization; a random character is used if null
        // Above is Used to 'prime' the LSTM with a character sequence to continue/complete.
        // Initialization characters must all be in CharacterIterator.getMinimalCharacterSet() by default


        //Get a DataSetIterator that handles vectorization of text into something we can use to train
        // our LSTM network.
        DataSetIterator iter = getIterator(miniBatchSize, exampleLength);

        //-----------------------------------------------------------
//        DataSet ds1 = iter.next();
//        INDArray indArray = ds1.getFeatures();
//        File f = new File(ROOT_PATH + "save.txt");
//        if (f.exists()) {
//            f.delete();
//            try {
//                f.createNewFile();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        OutputStream os = null;
//        try {
//            os = new FileOutputStream(f);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        int count = 0;
//        for (int i = 0; i < indArray.length(); i++) {
//            try {
//                byte[] b = floatToByteArray(indArray.getFloat(i));
//                os.write(b);
//                if (count % 1000 == 0) {
//                    os.flush();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            count++;
//        }
//        try {
//            os.flush();
//            os.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        InputStream is = null;
//        try {
//            is = new FileInputStream(f);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        File fWav = new File(ROOT_PATH + "out.mp3");
//        try {
//            WavBuilder.build3(is, fWav);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        System.exit(-1);
        //-----------------------------------------------------------


        int nOut = iter.totalOutcomes();
        int nIn = iter.inputColumns();

        System.out.println("nIn: " + nIn + " nOut:" + nOut);

        //Set up network configuration:
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(12345)
                .l2(0.0001)
                .weightInit(WeightInit.XAVIER)
                .updater(new Adam(0.005))
                .list()
                .layer(0, new LSTM.Builder().nIn(nIn).nOut(lstmLayerSize)
                        .activation(Activation.TANH).build())
                .layer(1, new LSTM.Builder().nIn(lstmLayerSize).nOut(lstmLayerSize)
                        .activation(Activation.TANH).build())
                .layer(2, new RnnOutputLayer.Builder(
                        LossFunctions.LossFunction.MCXENT).activation(Activation.SOFTMAX)
                        .nIn(lstmLayerSize).nOut(nOut).build())
                .backpropType(BackpropType.TruncatedBPTT)
                .tBPTTForwardLength(tbpttLength)
                .tBPTTBackwardLength(tbpttLength)
                .build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);

        File fNet = new File(pathForSaveNet);
        if (fNet.exists()) {
            try {
                net = MultiLayerNetwork.load(fNet, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Net loaded from " + fNet);
        } else {
            net.init();
        }
        net.setListeners(new ScoreIterationListener(1));

        //Print the  number of parameters in the network (and for each layer)
        Layer[] layers = net.getLayers();
        long totalNumParams = 0;
        for (int i = 0; i < layers.length; i++) {
            long nParams = layers[i].numParams();
            System.out.println("Number of parameters in layer " + i + ": " + nParams);
            totalNumParams += nParams;
        }
        System.out.println("Total number of network parameters: " + totalNumParams);

        //Do training, and then generate and print samples from network
        int miniBatchNumber = 0;
        for (int i = 0; i < numEpochs; i++) {
            while (iter.hasNext()) {
                DataSet ds = iter.next();
                net.fit(ds);
                if (++miniBatchNumber % generateSamplesEveryNMinibatches == 0) {
                    samples(generationInitialization, net, iter, nPricesToSample, nSamplesToGenerate);
                }
            }
            iter.reset();    //Reset iterator for another epoch
            try {
                net.save(fNet);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("\n\nExample complete");

    }

    private void samples(short[] initialization, MultiLayerNetwork net,
                         DataSetIterator iter, int samplesCount, int numSamples) {

        System.out.println("--------------------");
        float[] samples = sampleMusicFromNetwork(
                initialization, net, (MusicIterator) iter, samplesCount, numSamples);
//        for (int j = 0; j < samples.length; j++) {
//            System.out.println("----- Sample " + j + " -----");
//            String result = samples[j];
//            saveResult(result);
//            System.out.println(result);
//            System.out.println();
//        }
    }

    private static void saveResult(String result) {
//        String[] results = result.split(";");
//        String[] rs = results[0].split(SEPARATOR);
//        String[] frs = results[1].split(SEPARATOR);
//        File f = new File(ROOT_PATH + "out.txt");
//
//        try {
//            if (f.exists()) {
//                f.delete();
//            }
//            f.createNewFile();
//            FileWriter fw = new FileWriter(f);
//            for (String price : rs) {
//                fw.append(price.replace(".", ","));
//                fw.append("\n");
//            }
//            fw.append("\n");
//            for (String price : frs) {
//                fw.append(price.replace(".", ","));
//                fw.append("\n");
//            }
//            fw.flush();
//            fw.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }

    private static void clearResults() {
        File f = new File(ROOT_PATH + "out.txt");
        if (f.exists()) {
            f.delete();
        }
    }


    public static DataSetIterator getIterator(int batchSize, int sequenceLength) {
//        NativeAudioRecordReader recordReader = new NativeAudioRecordReader();
//        String[] formats = {"mp3"};
//        InputSplit inputSplit = new FileSplit(new File(ROOT_PATH + "samples/"), formats);
////        Configuration conf = new Configuration();
//
//        try {
//            recordReader.initialize(inputSplit);
//        } catch (IOException | InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        DataSetIterator iter = new RecordReaderDataSetIterator(recordReader, 1);
        MusicIterator iter = null;
        try {
            iter = new MusicIterator(ROOT_PATH + "samples/outLoad.mp3", batchSize, sequenceLength, random);
            return iter;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private float[] sampleMusicFromNetwork(short[] initialization,
                                           MultiLayerNetwork net,
                                           MusicIterator iter,
                                           int samplesCount,
                                           int numSamples) {

        if (initialization == null) {
            initialization = new short[1];
            initialization[0] = randomShort();
        }

        //Create input for initialization
        INDArray initializationInput = Nd4j.zeros(numSamples, iter.inputColumns(), initialization.length);
        short[] init = initialization;
        for (int i = 0; i < init.length; i++) {
            int idx = iter.convertSampleToIndex(init[i]);
            for (int j = 0; j < numSamples; j++) {
                initializationInput.putScalar(new int[]{j, idx, i}, 1.0f);
            }
        }
        int maxStartSamples = 3;
        List<Short>[] samples = new ArrayList[numSamples];
        List<List<Short>> fResults = new ArrayList<>();
        for (int i = 0; i < numSamples; i++) {
            samples[i] = new ArrayList<>();
            fResults.add(new ArrayList<>());
            short[] tmp;
            if (initialization.length > maxStartSamples) {
                tmp = Arrays.copyOfRange(initialization, initialization.length - maxStartSamples, initialization.length);
            } else {
                tmp = initialization;
            }
            for (short ip : tmp) {
                samples[i].add(ip);
            }
        }

        net.rnnClearPreviousState();
        INDArray output = net.rnnTimeStep(initializationInput);
        output = output.tensorAlongDimension((int) output.size(2) - 1, 1, 0);    //Gets the last time step output

        for (int i = 0; i < samplesCount; i++) {
            INDArray nextInput = Nd4j.zeros(numSamples, iter.inputColumns());
            for (int s = 0; s < numSamples; s++) {
                double[] outputProbDistribution = new double[iter.totalOutcomes()];
                for (int j = 0; j < outputProbDistribution.length; j++) {
                    outputProbDistribution[j] = output.getDouble(s, j);
                }
                int sampledPriceIdx = sampleFromDistribution(outputProbDistribution, rng);

                nextInput.putScalar(new int[]{s, sampledPriceIdx}, 1.0f);
                float f = iter.convertIndexToPrice(sampledPriceIdx);//Prepare next time step input
                samples[s].append(f).append(SEPARATOR);
                fResults.get(s).add(f);
            }

            output = net.rnnTimeStep(nextInput);    //Do one time step of forward pass
        }

        String[] out = new String[numSamples];
        for (int i = 0; i < numSamples; i++) {
            samples[i].setLength(samples[i].length() - 1);

            samples[i].append(";");
            float[] futures = future();
            List<Float> fr = fResults.get(i);
            for (int j = 0; j < futures.length; j++) {
                float deltaError = futures[j] - fr.get(j);
                samples[i].append(deltaError).append(SEPARATOR);
            }
            samples[i].setLength(samples[i].length() - 1);

            String sOut = samples[i].toString();
            out[i] = sOut;
        }
        return out;
    }

//    private static int convertFloatToIndex(float v) {
//
//    }

//
//    public static int sampleFromDistribution(double[] distribution, Random rng) {
//        double d = 0.0;
//        double sum = 0.0;
//        for (int t = 0; t < 10; t++) {
//            d = rng.nextDouble();
//            sum = 0.0;
//            for (int i = 0; i < distribution.length; i++) {
//                sum += distribution[i];
//                if (d <= sum) return i;
//            }
//            //If we haven't found the right index yet, maybe the sum is slightly
//            //lower than 1 due to rounding error, so try again.
//        }
//        //Should be extremely unlikely to happen if distribution is a valid probability distribution
//        throw new IllegalArgumentException("Distribution is invalid? d=" + d + ", sum=" + sum);
//    }


    public static byte[] floatToByteArray(float value) {
        byte[] bytes = new byte[4];
        ByteBuffer.wrap(bytes).putFloat(value);
        return bytes;
    }

    public static double toDouble(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getDouble();
    }

    private short randomShort() {
        if (random.nextFloat() > 0) {
            return (short) (random.nextFloat() * Short.MAX_VALUE);
        } else {
            return (short) (-random.nextFloat() * Short.MAX_VALUE);
        }
    }
}
