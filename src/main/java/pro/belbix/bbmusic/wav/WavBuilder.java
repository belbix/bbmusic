package pro.belbix.bbmusic.wav;

import com.sun.media.sound.WaveFileWriter;
import org.apache.commons.lang.ArrayUtils;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.FrameRecorder;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.bytedeco.ffmpeg.global.avutil.AV_SAMPLE_FMT_S16;

public class WavBuilder {

    public static void build(InputStream in, File f) throws IOException, UnsupportedAudioFileException {

        AudioInputStream inA = AudioSystem.getAudioInputStream(in);

        WaveFileWriter writer = new WaveFileWriter();
        writer.write(inA, AudioFileFormat.Type.WAVE, f);


    }


    public static void build2(String path, double[][] buffer) throws IOException, WavFileException {
        int sampleRate = 44100;    // Samples per second
        double duration = 5.0;     // Seconds

        // Calculate the number of frames required for specified duration
        long numFrames = (long) (duration * sampleRate);

        // Create a wav file with the name specified as the first argument
        WavFile wavFile = WavFile.newWavFile(new File(path), 2, numFrames, 16, sampleRate);

        // Create a buffer of 100 frames
//        double[][] buffer = new double[2][100];

        // Initialise a local frame counter
        long frameCounter = 0;

        // Loop until all frames written
//        while (frameCounter < numFrames) {
//            // Determine how many frames to write, up to a maximum of the buffer size
//            long remaining = wavFile.getFramesRemaining();
//            int toWrite = (remaining > 100) ? 100 : (int) remaining;
//
//            // Fill the buffer, one tone per channel
////            for (int s = 0; s < toWrite; s++, frameCounter++) {
////                buffer[0][s] = Math.sin(2.0 * Math.PI * 400 * frameCounter / sampleRate);
////                buffer[1][s] = Math.sin(2.0 * Math.PI * 500 * frameCounter / sampleRate);
////            }
//
//            // Write the buffer
//            wavFile.writeFrames(buffer, toWrite);
//        }
        wavFile.writeFrames(buffer, buffer.length);
        // Close the wavFile
        wavFile.close();
    }

    public static void build3(InputStream in, File file) throws IOException {
        System.out.println("Start build out");

        byte[] b = new byte[4];
        List<Float> floats = new ArrayList<>();
        int count = 0;
        while (true) {
            int size = in.read(b);
            if (size < 4) break;
            float f = byteArrToFloat(b);

            floats.add(f);
            count++;
            if ((count % (100000)) == 0) {
                System.out.println(Instant.now() + " Handle 100 000 floats");
            }
        }
        in.close();
        float[] farr = listToArray(floats);

//        writeArray(file, farr, 2);

        System.out.println("End build out");
    }

    public static void writeArray(File file, short[] arr, int channels) throws FrameRecorder.Exception {
        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(file, channels);
        recorder.setAudioBitrate(128000);
        recorder.setSampleFormat(AV_SAMPLE_FMT_S16);
        recorder.start();
        recorder.recordSamples(44100, channels, ShortBuffer.wrap(arr));
        recorder.stop();
    }

//    public static ByteArrayOutputStream mp3ToStream(String path) {
//        File f = new File(path);
//        AudioFormat audioFormat = new AudioFormat();
//
//        try (final InputStream is = new FileInputStream(f);
//             final AudioInputStream sourceAIS = AudioSystem.getAudioInputStream(is)) {
//            AudioFormat sourceFormat = sourceAIS.getFormat();
//            AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
//                    sourceFormat.getSampleRate(),
//                    16,
//                    sourceFormat.getChannels(),
//                    sourceFormat.getChannels() * 2,
//                    sourceFormat.getSampleRate(),
//                    false);
//            try (final AudioInputStream convert1AIS = AudioSystem.getAudioInputStream(convertFormat, sourceAIS);
//                 final AudioInputStream convert2AIS = AudioSystem.getAudioInputStream(audioFormat, convert1AIS);
//                 final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
//                byte[] buffer = new byte[8192];
//                while (true) {
//                    int readCount = convert2AIS.read(buffer, 0, buffer.length);
//                    if (readCount == -1) {
//                        break;
//                    }
//                    baos.write(buffer, 0, readCount);
//                }
//                return baos;
//            }
//        } catch (UnsupportedAudioFileException | IOException e) {
//            e.printStackTrace();
//        }
//    }

    private static float byteArrToFloat(byte[] arr) {
        return ByteBuffer.wrap(arr).getFloat();
//        return Float.intBitsToFloat(arr[0] ^ arr[1] << 8 ^ arr[2] << 16 ^ arr[3] << 24);
    }

    private static float[] listToArray(List<Float> floats){
        float[] arr = new float[floats.size()];
        int count = 0;
        for(Float aFloat: floats){
            arr[count] = aFloat;
            count++;
        }
        return arr;
    }
}


