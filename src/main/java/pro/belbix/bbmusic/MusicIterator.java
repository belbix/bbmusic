package pro.belbix.bbmusic;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static pro.belbix.bbmusic.LoadMusic.loadData;

public class MusicIterator implements DataSetIterator {
    private final static int MIN_SAMPLE = Short.MIN_VALUE;
    private final static int MAX_SAMPLE = Short.MAX_VALUE;
    private final static int COUNT_SAMPLES = (MAX_SAMPLE - MIN_SAMPLE) / 10;
//        private final static int COUNT_SAMPLES = 10;
    private final String filePath;
    private final int batchSize;
    private final int exampleLength;
    private final Random random;
    private TreeMap<Short, Integer> sampleToIdxMap;
    private short[] validSamples;
    private short[] fileSamples;
    private LinkedList<Integer> exampleStartOffsets = new LinkedList<>();

    public MusicIterator(String filePath, int batchSize, int exampleLength, Random random) throws IOException {
        this.filePath = filePath;
        this.batchSize = batchSize;
        this.exampleLength = exampleLength;
        this.random = random;
        init();
    }

    private void init() throws IOException {
        File f = new File(filePath);
        if (!new File(filePath).exists())
            throw new IOException("Could not access file (does not exist): " + filePath);
        List<Short> writables = loadData(f, null);

        sampleToIdxMap = initIdx(MIN_SAMPLE, MAX_SAMPLE, COUNT_SAMPLES);


        short[] hzes = new short[writables.size()];
        int currIdx = 0;
        for (Short hz : writables) {
            sampleToIdx(hz); //for validate hz
            hzes[currIdx++] = hz;
        }

        if (currIdx == hzes.length) {
            fileSamples = hzes;
        } else {
            fileSamples = Arrays.copyOfRange(hzes, 0, currIdx);
        }

        if (exampleLength >= fileSamples.length)
            throw new IllegalArgumentException("exampleLength=" + exampleLength
                    + " cannot exceed number of valid prices in file (" + fileSamples.length + ")");

        System.out.println("Loaded and converted file: " + fileSamples.length + " valid Hz");
        initializeOffsets();
    }

    public TreeMap<Short, Integer> initIdx(int min, int max, int count) {
        TreeMap<Short, Integer> ret = new TreeMap<>();
        validSamples = new short[count];
        float delta = max - min;
        float batchSize = delta / (float) count;
        for (int i = 0, el = (short) (min + batchSize); i < count; i++) {
            ret.put((short) el, i);
            validSamples[i] = (short) el;
            el += batchSize;
        }
        return ret;
    }

    private short sampleToIdx(short sample) {
        if (sample < sampleToIdxMap.firstKey())
            throw new RuntimeException("Invalid hz " + sample);
        short lastKey = -1;
        for (short key : sampleToIdxMap.keySet()) {
            if (sample < key) {
                break;
            }
            lastKey = key;
        }
        if (lastKey == -1)
            throw new RuntimeException("Invalid hz " + sample);
        return lastKey;
    }

    private void initializeOffsets() {
        //This defines the order in which parts of the file are fetched
        int nMinibatchesPerEpoch = (fileSamples.length - 1) / exampleLength - 2;   //-2: for end index, and for partial example
        if (nMinibatchesPerEpoch < 1)
            throw new RuntimeException("Too small data " + fileSamples.length + " for exampleLength " + exampleLength + "" +
                    ". Need modulo + 2 > 1");
        for (int i = 0; i < nMinibatchesPerEpoch; i++) {
            exampleStartOffsets.add(i * exampleLength);
        }
        Collections.shuffle(exampleStartOffsets, random);
    }

    public int convertSampleToIndex(short c) {
        return sampleToIdxMap.get(sampleToIdx(c));
    }

    public float convertIndexToSample(int idx) {
        return validSamples[idx];
    }

    @Override
    public DataSet next(int num) {
        if (exampleStartOffsets.size() == 0) throw new NoSuchElementException();

        int currMinibatchSize = Math.min(num, exampleStartOffsets.size());
        //Allocate space:
        //Note the order here:
        // dimension 0 = number of examples in minibatch
        // dimension 1 = size of each vector (i.e., number of characters)
        // dimension 2 = length of each time series/example
        //Why 'f' order here? See http://deeplearning4j.org/usingrnns.html#data section "Alternative: Implementing a custom DataSetIterator"
        INDArray input = Nd4j.create(new int[]{currMinibatchSize, sampleToIdxMap.size(), exampleLength}, 'f');
        INDArray labels = Nd4j.create(new int[]{currMinibatchSize, sampleToIdxMap.size(), exampleLength}, 'f');

        for (int i = 0; i < currMinibatchSize; i++) {
            int startIdx = exampleStartOffsets.removeFirst();
            int endIdx = startIdx + exampleLength;
            short currSample = fileSamples[startIdx];
            int currPriceIdx = sampleToIdxMap.get(sampleToIdx(currSample));    //Current input
            int c = 0;
            for (int j = startIdx + 1; j < endIdx; j++, c++) {
                short nextSample = fileSamples[j];
                int nextPriceIdx = sampleToIdxMap.get(sampleToIdx(nextSample));        //Next character to predict
                input.putScalar(new int[]{i, currPriceIdx, c}, 1.0);
                labels.putScalar(new int[]{i, nextPriceIdx, c}, 1.0);
                currPriceIdx = nextPriceIdx;
            }
        }

        return new DataSet(input, labels);
    }

    @Override
    public int inputColumns() {
        return validSamples.length;
    }

    @Override
    public int totalOutcomes() {
        return validSamples.length;
    }

    @Override
    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public void reset() {
        exampleStartOffsets.clear();
        initializeOffsets();
    }

    @Override
    public int batch() {
        return batchSize;
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<String> getLabels() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean hasNext() {
        return exampleStartOffsets.size() > 0;
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }
}
