package pro.belbix.bbmusic;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.datavec.api.writable.IntWritable;
import org.datavec.api.writable.Writable;
import pro.belbix.bbmusic.wav.WavBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.ffmpeg.global.avutil.AV_SAMPLE_FMT_FLT;
import static org.bytedeco.ffmpeg.global.avutil.AV_SAMPLE_FMT_S16;

public class LoadMusic {
    private static final String ROOT_PATH = "/trainsets/music/";

    public static void main(String[] args) throws IOException {

        File f = new File(ROOT_PATH + "imp_march.mp3");
        List<Short> writables = loadData(f, null);

        File out = new File(ROOT_PATH + "outLoad.mp3");
//        build(writables, out);
        WavBuilder.writeArray(out, writablesToArray(writables), 1);
        System.out.println("END");
    }

    public static void build(List<Writable> writables, File file) throws IOException {

        System.out.println("Start build out");
        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(file, 2);
//        recorder.setSampleFormat(AV_SAMPLE_FMT_FLT);
//        recorder.setAudioCodec(AV_CODEC_ID_MP3);
        recorder.setAudioBitrate(128000);
        recorder.start();
        int[] farr = new int[writables.size()];
        int count = 0;
        for (Writable writable : writables) {
            int f = ((IntWritable) writable).get();
            farr[count] = f;
            count++;
        }
        Buffer buffer = IntBuffer.wrap(farr);
        recorder.recordSamples(44100, 2, buffer);

        recorder.flush();
        recorder.stop();

        System.out.println("End build out");
    }

    public static List<Short> loadData(File file, InputStream inputStream) throws IOException {
        List<Short> ret = new ArrayList<>();
        try (FFmpegFrameGrabber grabber = inputStream != null ? new FFmpegFrameGrabber(inputStream)
                : new FFmpegFrameGrabber(file.getAbsolutePath())) {
            grabber.setSampleFormat(AV_SAMPLE_FMT_S16);
            grabber.start();
            Frame frame;
            while ((frame = grabber.grab()) != null) {
                while (frame.samples != null && frame.samples[0].hasRemaining()) {
                    for (int i = 0; i < frame.samples.length; i++) {
                        ret.add(((ShortBuffer) frame.samples[i]).get());
                    }
                }
            }
        }
        return ret;
    }

    public static short[] writablesToArray(List<Short> writables) {
        int size = writables.size() / 25;
        size /= 2;
        short[] arr = new short[size];
        short first = 0;
        for (int i = 0; i < (size * 2); i++) {
            short f =  writables.get(i);
            if (i % 2 == 0) {
                first = f;
                continue;
            }
            arr[i / 2] = (short)((first + f) / 2);
        }
        return arr;
    }

}
